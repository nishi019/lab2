import 'package:flutter/material.dart';

class Singleton {
  static final Singleton _singleton = Singleton._internal();

  factory Singleton() {
    return _singleton;
  }

  Singleton._internal();
  Color background = Colors.green;
  Color selectedItem = Colors.white;
  Color unselectedItem = Colors.white54;
}