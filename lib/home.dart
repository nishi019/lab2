
import 'package:cs481_lab2/ui/home_screen.dart';
import 'package:cs481_lab2/ui/profile_screen.dart';
import 'package:cs481_lab2/ui/setting_screen.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map<String, Widget> navigationItemBottomBar = const {
    "Home": Icon(Icons.home),
    "Tab 1": Icon(Icons.info),
    "Setting": Icon(Icons.settings)
  };

  @override
  void initState() {
    super.initState();
  }

  Map<int, Widget> navigationBody = {
    0: HomeScreen(),
    1: ProfileScreen(),
    2: SettingScreen(),
  };

  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("App Lab2")),
      body: navigationBody[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: currentIndex,
          backgroundColor: Colors.green,
          elevation: 0.0,
          onTap: updateCurrentIndex,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white54,
          items: navigationItemBottomBar.entries.map( (e) =>
              BottomNavigationBarItem(title: Text(e.key), icon: e.value),).toList()),
    );
  }

  void updateCurrentIndex(int index) {
    setState(() { currentIndex = index; });
  }

}
