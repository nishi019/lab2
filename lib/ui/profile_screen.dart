import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {

  var _yourchoice = ' ';

  void displayBottomSheet() {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: Icon(Icons.account_circle),
              title: Text("Edit profile picture"),
              onTap: () {
                setState(() {_yourchoice = 'Edit profile picture';});
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.edit),
              title: Text("Edit profile contents"),
              onTap: () {
                setState(() {_yourchoice = 'Edit profile contents';});
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.share),
              title: Text("Share"),
              onTap: () {
                setState(() {_yourchoice = 'Share your profile';});
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.delete),
              title: Text("Delete"),
              onTap: () {
                setState(() {_yourchoice = 'Delete your profile';});
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text("Welcome to the profile tab", style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                ),),
              RaisedButton(
                child: Text("Options"),
                onPressed: displayBottomSheet,
              ),
              Text(_yourchoice, style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),)
            ],
          ),
        ),
      ),
    );
  }
}